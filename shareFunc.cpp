#include "Node_PPort_Edge_cluster.h"
#include "DRC.h"


/**
 * judge whether the block is sequential or not
 * @param block to be judge
 * @return a bool value
 */
bool isSequentialBlock( Block *block )
{
    int defId = block->getBlockDefId();
    return (defId >= BLOCK_CS_REG_PRIM && defId <= BLOCK_CS_REG_RS_PRIM);
}

/**
 * judge whether the block is combinational or not
 * @param block to be judge
 * @return a bool value
 */
bool isCombinationalBlock( Block *block )
{
    if ( block->getBlockDef()->linkedNetwork() != NULL ) {
        return true;
    }

    return !isSequentialBlock(block);
}


//considering the BPorts are not that much, use O(n2) compare
bool DRCinf::DRCcheck(DRCinf& inNetwork)
{
	if(numBport != inNetwork.numBport)
	{
		return false;
	}
	else
	{
		for(unsigned int count_x=0;count_x<networkBp.size();count_x++)
		{
			bool hasSame(false);
			for(unsigned int count_y=0;count_y<inNetwork.networkBp.size();count_y++)
			{
				if((inNetwork.networkBp[count_y].BportName == networkBp[count_x].BportName)&&(inNetwork.networkBp[count_y].nodeDir == networkBp[count_x].nodeDir))
				{
					hasSame = true;
					break;
				}
			}
			if(hasSame == false)
			{
				return false;
			}
		}
		return true;
	}




}

void DRCinf::DRCinfCreate(Network* networkIn)
{

		numBport = networkIn->getNumPorts();
		GeneralIterator<Port*>allBports = networkIn->getAllPorts();
		while(allBports.hasNext())
		{
			Port* allBport = allBports.next();
			string name = allBport->getName();
			BportInfor tempBp(name);
			int direction = allBport->getDirection();
			if(direction == 1)
			{
				tempBp.nodeDir = dirIn;
			}
			else if(direction ==2)
			{
				tempBp.nodeDir =dirOut;
			}
			else
			{
				tempBp.nodeDir =dirInOut;
			}
			networkBp.push_back(tempBp);
		}
}




#include"Merge_func.h"



static bool isGNDNet(Net* net)
{
    if (net && net->getSrcPort())
    {
        Block* srcBlock = net->getSrcPort()->getBlock();
        if ( srcBlock && srcBlock->getBlockDef()->getName() == "GND" )
        {
            return true;
        }
    }

    return false;
}




bool netlistChecker(Network *network) {
    // valid check
    bool res = false;
    GeneralIterator<Net*> nets = network->getAllNets();
    while (nets.hasNext()) {
        Net *net = nets.next();
        // check src port
        Port * srcPort = net->getSrcPort();
        if (srcPort)
        {
            if (srcPort->getNet() != net) {
                cout << "Invalid Net: " << net->getName()
                    << "\tSource port connect to other net."
                    << endl;
                res = true;
            }
        }
        
        GeneralIterator<Port*> dstPorts = net->getDstPorts();
        while (dstPorts.hasNext()) {
            Port *dstPort = dstPorts.next();
            if (dstPort->getNet() != net) {
                cout << "Invalid Net: " << net->getName()
                    << "\tDstination port connect to other net."
                    << endl;
                res = true;
            }
        }
    }
    return res;
}

bool moveNetToNetwork(Net *net, Network *srcNetwork, Network *dstNetwork) {
    if ( !net ) {
        cout << "net is NULL, can not move to network." << endl;
        return false;
    }

    if ( !dstNetwork ) {
        cout << "source network is NULL, can not move net to it." << endl;
        return false;
    }

    if ( !dstNetwork ) {
        cout << "destination network is NULL, can not move net to it." << endl;
        return false;
    }

    if ( srcNetwork->getNet(net->getId()) == NULL ) {
        cout << "net is not in source network, can not move it to dstNetwork." << endl;
        return false;
    }
    try {
        srcNetwork->removeNet(net);
        dstNetwork->addNet(net);
    }
    catch (AgateException &exp)
    {
        cout << exp.getMessage() << endl;
        return false;
    }
    return true;
}

bool movePortToNetwork(Port *port, Network *dstNetwork) {
    if ( !port ) {
        cout << "port is NULL, can not move to network." << endl;
        return false;
    }

    if ( !dstNetwork ) {
        cout << "destination network is NULL, can not move port to it." << endl;
        return false;
    }

    Network *srcNetwork = dynamic_cast<Network *>(port->getOwner());

    try
    {
        if ( !srcNetwork ) {
            // this is a new port, it belongs to NULL, just addPort to dstNetwork
            dstNetwork->addPort(port);
        }
        else {
            // need remove port from source network first, then addPort to dstNetwork
            srcNetwork->removePort(port);
            dstNetwork->addPort(port);
        }
    }
    catch (AgateException &exp)
    {
        cout << exp.getMessage() << endl;
        return false;
    }
    return true;
}

bool moveBlockToNetwork(Block *block, Network *dstNetwork) {
    if ( !block ) {
        cout << "block is NULL, can not move to network." << endl;
        return false;
    }

    if ( !dstNetwork ) {
        cout << "destination network is NULL, can not move block to it." << endl;
        return false;
    }

    Network *srcNetwork = block->getOwner();

    try {
        if ( !srcNetwork ) {
            // this is a new block, it belongs to NULL, just addBlock to dstNetwork
            dstNetwork->addBlock(block);
        }
        else {
            // need remove block from source network first, then addBlock to dstNetwork
            srcNetwork->removeBlock(block);
            dstNetwork->addBlock(block);
        }
    }
    catch (AgateException &exp)
    {
        cout << exp.getMessage() << endl;
        return false;
    }

    return true;
}


void flattenNetwork(Network* network)
{
    // first need to flatten the Network
    GeneralIterator<Block*> blocks = network->getAllBlocks();
    vector<Block*> flattenBlockList;
    while (blocks.hasNext())
    {
        Block *block=blocks.next();
        Network *linkedNetwork = block->getBlockDef()->linkedNetwork();

        if ( linkedNetwork ) {
            if ( linkedNetwork->getNumBlocks() > 0 || linkedNetwork->getNumNets() > 0 ) {
                flattenBlockList.push_back(block);
            }
        }
    }
    for (unsigned int i = 0; i < flattenBlockList.size(); i++)
    {
        //flatten the children first, then flatten itself
        flattenNetwork(flattenBlockList[i]->getBlockDef()->linkedNetwork());
        Network* linkedNetworkClone = flattenBlockList[i]->getBlockDef()->linkedNetwork()->deepClone();
        NetUtil::flattenBlackBox(network, flattenBlockList[i]->getId(), linkedNetworkClone);
        delete linkedNetworkClone;
    }
}

Network* read_subnetwork(const char* topModule,const char* fileName,ExtendDefManager* extDefManager )
{
    const char *libFileName = "generic.v" ;
    StandardDefManager* stdDefManager = StandardDefManager::getInstance();
    ifstream libStream(libFileName);
    if (libStream.fail()) { // read file faild
        cout << "Could not open lib file: " << libFileName << endl;
        exit(1);
    }

    if (DefBuilder::build(stdDefManager, libStream) == false) {
        cout << "Failed to build library: " << libFileName << endl;
        libStream.close();
        exit(1);
    }
    libStream.close();

//	ExtendDefManager* extDefManager = new ExtendDefManager();
    Network* sub_network = 0;
    list<AString> designFileList;
    designFileList.push_back(fileName);
//	 ExtendDefManager* extDefManager2 = new ExtendDefManager();
    sub_network = NetworkReader::readVerilog(designFileList,
        topModule, extDefManager, false);

    if (sub_network == 0) {
        cout << "parse verilog file failed." << endl;
        return false;
        // if parse verilog file failed, you also can create a network
        // to enable this, please comment the above "return -1" statement
        //network = new Network(topModule, extDefManager); // this network has no port/block/net
    }

    flattenNetwork(sub_network);
    return sub_network;
}

Network* merge(vector<Network*>&subNetworks,vector<Cutpair>&cut_pair,const string &mergedNetworkTopModule,ExtendDefManager* extDefManager)
{
	bool checkMerge(false);

	checkMerge=	netlistChecker(subNetworks[0]);
	checkMerge=	netlistChecker(subNetworks[1]);
	checkMerge=	netlistChecker(subNetworks[2]);
	checkMerge=	netlistChecker(subNetworks[3]);

//-----------------------------Second step: Construct the Merged Network, which contains all Boundry Ports,Nets, Blocks( Cause changing the Owner directly,  after constructing, the Merged Network is connected inside)
    string Merged_name = mergedNetworkTopModule;
    Network* Merged_Network = new Network(Merged_name,extDefManager);
    for(unsigned int count_x=0;count_x<subNetworks.size();count_x++)
    {

        //Cause a same name Block B may exist before put Block A into Merged Network, should check this situation
        GeneralIterator<Block*>O_blocks = subNetworks[count_x]->getAllBlocks();
        while(O_blocks.hasNext())
        {
            Block* O_block = O_blocks.next();
            string O_block_name = O_block->getName().str();
            string O_block_new_name = O_block_name;
            int postfix(0);
            Block* M_block = Merged_Network->getBlock(O_block_new_name);
            while(M_block != NULL)
            {
                postfix++;
                char id[10];
                sprintf(id,"%d",postfix);
                string ID = id;
                O_block_new_name = O_block_new_name + ID;
                M_block = Merged_Network->getBlock(O_block_new_name);
            }
            subNetworks[count_x]->renameBlock(O_block_name,O_block_new_name);
            moveBlockToNetwork(O_block,Merged_Network);
        }

        //Cause Boundry Port has unique name, does not need to check whether a same name Bport exist
        GeneralIterator<Port*>O_boundryPorts = subNetworks[count_x]->getAllPorts();
        while(O_boundryPorts.hasNext())
        {
            Port* O_boundryPort = O_boundryPorts.next();
			string BportName = O_boundryPort->getName();
			Port* M_boundryPort = Merged_Network->getPort(BportName);
			if(M_boundryPort == NULL)
			{
				movePortToNetwork(O_boundryPort,Merged_Network);
			}
			else
			{
				Net* mNet = M_boundryPort->getNet();
				Net* oNet = O_boundryPort->getNet();
				if((mNet ==NULL)&&(oNet != NULL))
				{
					Merged_Network->deletePort(M_boundryPort);
					movePortToNetwork(O_boundryPort,Merged_Network);
				}
				else if((mNet!=NULL)&&(oNet!=NULL))
				{
					if((isGNDNet(mNet)==true)&&(isGNDNet(oNet)==false))
					{
						Merged_Network->deletePort(M_boundryPort);
						movePortToNetwork(O_boundryPort,Merged_Network);
					}
					else if((isGNDNet(mNet)==false)&&(isGNDNet(oNet)==false))
					{
				//		repetitionBport<<BportName<<endl;
						cout<<"jjjjjjjjjjjjjjjjjjjjjj"<<endl;
						cout<<BportName<<endl;
					}
				}
			}
        }
        //Cause a same name Net B may exist before put Net A into Merged Network, should check this situation
        GeneralIterator<Net*>O_Nets = subNetworks[count_x]->getAllNets();
        while(O_Nets.hasNext())
        {
            Net* O_Net = O_Nets.next();
            string O_Net_name = O_Net->getName().str();
            string O_Net_new_name = O_Net_name;
            Net*M_Net = Merged_Network->getNet(O_Net_new_name);
            int postfix(0);

            while(M_Net != NULL)
            {
                postfix++;
                char id[10];
                sprintf(id,"%d",postfix);
                string ID = id;
                O_Net_new_name = O_Net_new_name + ID;
                M_Net = Merged_Network->getNet(O_Net_new_name);
            }
            subNetworks[count_x]->renameNet(O_Net_name,O_Net_new_name);
            moveNetToNetwork(O_Net,subNetworks[count_x],Merged_Network);
        }
	}

	checkMerge=	netlistChecker(Merged_Network);
    cout << "third step..." << endl;
    cout<<"Total Port Num: "<<Merged_Network->getNumPorts()<<endl;
//-------------------Third Step, Merge the cut position through the Cut pair Info-------------------------------------------------
//-------------------2014/11/3 Ver  : in this version, No anticipate on each cut pair, so no constraints----------------------------
    int numCutpair(0);
    numCutpair = cut_pair.size();
    cout<<"Total Cut pair: "<<numCutpair<<endl;
    for(unsigned int count_x=0;count_x<cut_pair.size();count_x++)
    {
        Port* Boundry_PI = Merged_Network->getPort(cut_pair[count_x].PI_name);
        Port* Boundry_PO = Merged_Network->getPort(cut_pair[count_x].PO_name);
        if((Boundry_PI == NULL)||(Boundry_PO == NULL))
        {
            cout<<"wrong in I"<<endl;
        }
		if((Boundry_PI->getNet() == NULL)||(Boundry_PO->getNet() == NULL))
		{
			Merged_Network->deletePort(Boundry_PI);
			Merged_Network->deletePort(Boundry_PO);
			/*
			if(Boundry_PI->getNet() == NULL)
			{
			cout<<"PI name: "<< Boundry_PI->getName()<<endl;
			Merged_Network->deletePort(Boundry_PI);
			}
			if(Boundry_PO->getNet() == NULL)
			{
			cout<<"PO name: "<<Boundry_PO->getName()<<endl;
			Merged_Network->deletePort(Boundry_PO);
			}
			//cout<<"wrong in G"<<endl;
			*/
		}
		else
		{
			Net* PI_net = Boundry_PI->getNet();
			Net* PO_net = Boundry_PO->getNet();
            if((PI_net ==NULL)||(PO_net == NULL))
            {
                cout<<"wrong in H"<<endl;
            }
            PO_net->disconnectPort(Boundry_PO);
            GeneralIterator<Port*>PI_netDstPorts = PI_net->getDstPorts();
            vector<Port*>allDstPorts;
            while(PI_netDstPorts.hasNext())
            {
                Port* PI_netDstPort = PI_netDstPorts.next();
                allDstPorts.push_back(PI_netDstPort);
            }
            if(allDstPorts.size() == 0)
            {
                cout<<"The net:"<<PI_net->getName()<<"  has no DstPort"<<endl;
            }
            for(unsigned int count_z=0;count_z<allDstPorts.size();count_z++)
            {

                PI_net->disconnectPort(allDstPorts[count_z]);
				PO_net->addPort(allDstPorts[count_z]);
            }
            Merged_Network->deleteNet(PI_net);
            Merged_Network->deletePort(Boundry_PI);
            Merged_Network->deletePort(Boundry_PO);
        }
    }
	checkMerge = netlistChecker(Merged_Network);

        cout << "merge done..." << endl;
        return Merged_Network;
}

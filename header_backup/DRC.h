// The member functions' definition are all in the shareFunc.cpp

#ifndef _DRC_H
#define _DRC_H

#include "Node_PPort_Edge_cluster.h"
enum BpDir{dirIn,dirOut,dirInOut};

USING_NETLIST_NAMESPACE
class BportInfor
{
public:
	string BportName;
	BpDir nodeDir;
	BportInfor(string bpName):BportName(bpName){};
};

class DRCinf{
public:
	vector<BportInfor>networkBp;
	int numBport;
	bool DRCcheck(DRCinf& inNetwork);
	DRCinf():numBport(0){};
	DRCinf(Network* networkIn){
		numBport = networkIn->getNumPorts();
		GeneralIterator<Port*>allBports = networkIn->getAllPorts();
		while(allBports.hasNext())
		{
			Port* allBport = allBports.next();
			string name = allBport->getName();
			BportInfor tempBp(name);
			int direction = allBport->getDirection();
			if(direction == 1)
			{
				tempBp.nodeDir = dirIn;
			}
			else if(direction ==2)
			{
				tempBp.nodeDir =dirOut;
			}
			else
			{
				tempBp.nodeDir =dirInOut;
			}
			networkBp.push_back(tempBp);
		}
	}
	void DRCinfCreate(Network* networkIn);



};
#endif
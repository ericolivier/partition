#ifndef _NODE_PPORT_EDGE_CLUSTER_H
#define _NODE_PPORT_EDGE_CLUSTER_H

#include "utl_astring.h"    // AString
#include "net_defmanager.h" // StandardDefManager/ExtendDefManager definition
#include "def_builder.h"    // DefBuilder
#include "nlh_interface.h"  // NetworkReader
#include "net_network.h"    // Network, Attribute
#include "net_port.h"       // Port
#include "net_net.h"        // Net
#include "net_block.h"      // Block
#include "utl_generaliterator.h"    // GeneralIterator, for traversing ports/nets/blocks of network
#include "net_networkutility.h"     // NetUtil::flattenBlackBox
#include "common.h"
//#include "Node_PPort_Edge_cluster.h"


#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <vector>
#include <time.h>
USING_NETLIST_NAMESPACE

class PPort;
class Edge;
class Subnetwork;
class Node
{
public:
    Block* block;
    Block* new_block;
    string Node_name;
    bool is_comb;
    vector<Port*>input_port;
    vector<Port*>output_port;
    vector<Port*>inout_port;
    vector<PPort*>in_pport;
    vector<PPort*>out_pport;
    vector<PPort*>inout_pport;
    vector<PPort*>all_pport;
    bool check;
    //Node constructoror
    Node():check(false),is_comb(false),Node_name(""),new_block(NULL){}
    Node(Block* block_in):check(false),is_comb(false),Node_name(""),new_block(NULL){}


};
class PPort
{
public:
    bool isBoundryPPort;
//	bool directionIn;
    string PPort_name;
    Node* srcNode;
    Port* port;
    Port* new_port;
    Edge* edge;
//	Subnetwork* srcSubnetwork;
    bool check;

    PPort():check(false),isBoundryPPort(false),edge(NULL),port(NULL),srcNode(NULL),PPort_name(""),new_port(NULL){}
    PPort(Port* port_in):check(false),isBoundryPPort(false),edge(NULL),srcNode(NULL),new_port(NULL)
    {
        port = port_in;
        PPort_name = port_in->getName();
    }
};

class Edge
{
public:
    bool check;
    bool isNewAdd;
    string Edge_name;
    Net* net;      //link to Original network's net
    Net* new_net; // link to subnetwork's net
    vector<Port*>dst_port;
    Port* src_port;
    vector<PPort*>dst_pport;
    PPort* src_pport;
    vector<PPort*>all_pport;
    int Num_for_cut;

    Edge(Net* net_in):check(false),src_pport(NULL),Num_for_cut(0),Edge_name(""),new_net(NULL),isNewAdd(false){}
    Edge():check(false),src_pport(NULL),Num_for_cut(0),Edge_name(""),new_net(NULL),isNewAdd(false){}
};

class Cluster
{
    public:
    vector<Node*>node;
    vector<Edge*>edge;
    vector<PPort*>boundry_pport;
    bool need_map;
    int num_node;
    int num_net;
    Cluster():num_node(0),num_net(0),need_map(false){}
};

 class Cutpair
{
public:
    string PI_name;
    string PO_name;
    PPort* PI_pport;
    PPort* PO_pport;
    string new_edge_name;
    bool New_edge_connect_PI_pport;

    Cutpair():PI_name(""),PO_name(""),PI_pport(NULL),PO_pport(NULL),New_edge_connect_PI_pport(false){}

};

class Subnetwork
{
public:
    vector<Cluster*>member_cluster;
    int total_node;
    Subnetwork():total_node(0){}
};

bool isSequentialBlock( Block *block );
bool isCombinationalBlock( Block *block );

#endif

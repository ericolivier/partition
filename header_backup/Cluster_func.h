#ifndef _CLUSTER_FUNC
#define CLUSTER_FUNC

#include"Node_PPort_Edge_cluster.h"


vector<Cluster*> Sorting(vector<Cluster*>&p_cluster);
void LPT_partition(unsigned int num_processor,vector<Subnetwork>&partition_in,vector<Cluster>&Cluster_in);
PARTITION_DLLFUNC void cluster(Network* network,vector<Cutpair>&cutPairs,vector<Network*>&subNetworks,int partitionRuleSelection = 0 );

bool wirte_subnetwork(vector<Network*>sub_network,vector<string>&subnetwork_fileName);

#endif

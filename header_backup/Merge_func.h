#ifndef _MERGE_FUNC
#define _MERGE_FUNC

#include"Node_PPort_Edge_cluster.h"


Network* read_subnetwork(const char* topModule,const char* fileName,ExtendDefManager* extDefManager);

PARTITION_DLLFUNC Network* merge(vector<Network*>&subNetworks,vector<Cutpair>&cut_pair,const string &mergedNetworkTopModule,ExtendDefManager* extDefManager);

PARTITION_DLLFUNC void flattenNetwork(Network* network);
bool moveBlockToNetwork(Block *block, Network *dstNetwork);
bool movePortToNetwork(Port *port, Network *dstNetwork);
bool moveNetToNetwork(Net *net, Network *srcNetwork, Network *dstNetwork);






#endif
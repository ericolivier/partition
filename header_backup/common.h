#ifndef _COMMON_H_
#define _COMMON_H_
// recommend add this define in the the project property
//#define PARTITION_MAKEDLL

#ifdef _MSC_VER
#   if defined(PARTITION_NODLL)
#       undef PARTITION_MAKEDLL
#       define PARTITION_DLLFUNC
#   elif defined(PARTITION_MAKEDLL)
#       define PARTITION_DLLFUNC __declspec(dllexport)
#   else
#       define PARTITION_DLLFUNC __declspec(dllimport)
#   endif
#endif

#ifdef __GNUC__
#   define PARTITION_DLLFUNC
#endif

#endif
